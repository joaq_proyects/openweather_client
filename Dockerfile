FROM python:3-onbuild
RUN pip install -r requirements.txt
EXPOSE 8008
ADD . /src
COPY /src .
WORKDIR .
CMD ["gunicorn", "-b", "0.0.0.0:8008", "main"]
