create database weather_data;

use weather_data;

create table data_updates
(
	id int auto_increment,
	dt datetime,
	primary key(id)
);

create table cities
(
	id int auto_increment,
	api_city_id int unique,
	name varchar(100),
	longitude float,
	latitude float,
	country varchar(100),
	primary key(id)
);

create table cities_weather_updates
(
	id int auto_increment,
	api_city_id int,
	data_updates_id int,
	sunrise datetime,
	sunset datetime,
	weather_main varchar(50),
	weather_desc varchar(100),
	icon varchar(20),
	temp float,
	pressure float,
	humidity float,
	temp_min float,
	temp_max float,
	visibility float,
	wind_speed float,
	wind_deg float,
	clouds float,
	rain float,
	primary key(id),
	foreign key(api_city_id) references cities(api_city_id),
	foreign key(data_updates_id) references data_updates(id)
);
