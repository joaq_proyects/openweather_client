import weather_data, settings

if __name__ == "__main__":

	service_url = settings.service_url
	db_data = settings.db_data

	wd = weather_data.WeatherData(service_url,db_data)

	wd.data_loader()
