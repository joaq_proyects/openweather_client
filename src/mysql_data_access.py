
import pymysql

class MySqlDataAcces():
    host = None
    user = None
    pssw = None
    db = None
    charset = None
    conn = None

    def __init__(self, host,user,pssw,db):
        self.host = host
        self.user = user
        self.pssw = pssw
        self.db = db
        self.charset = 'utf8mb4'

    def connect(self):
        conn = pymysql.connect(
            host=self.host,
            user=self.user,
            password=self.pssw,
            db=self.db,
            charset='utf8mb4',
            cursorclass=pymysql.cursors.DictCursor
        )
        self.conn = conn

    # execute inserts or updates
    def exec_non_query(self, non_query, auto_commit = True):
        cursor = self.conn.cursor()
        cursor.execute(non_query)
        if(auto_commit):
            self.conn.commit()

    def exec_query_one(self, query):
        cursor = self.conn.cursor()
        cursor.execute(query)
        return cursor.fetchone()

    # Check if a reg exists
    def check_reg_existance(self, table, id_col, id_val):
        query = "select 1 as res from %s where %s = %s" % ( str(table), str(id_col), str(id_val) )
        resp = self.exec_query_one(query)
        result = '0'
        try:
            result = resp["res"]
        except:
            pass
        return ( str(result) == '1' )
