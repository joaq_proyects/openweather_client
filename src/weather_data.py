
import datetime, json
import requests, tzlocal
import mysql_data_access as mda


class WeatherData():
    service_url = None
    data_layer = None

    def __init__(self, url, db_data):
        self.service_url = url
        self.data_layer = mda.MySqlDataAcces(db_data["host"], db_data["user"], db_data["pssw"], db_data["db"])
        self.data_layer.connect()

    def data_loader(self, log_enable = False):
        req = requests.get(self.service_url)
        if(req.status_code != 200):
            return "Error %s executing %s: %s " % (str(req.status_code), self.service_url, str(req.text))

        resp = json.loads(req.text)

        if(log_enable):
            print("json response: " + str(resp), end="\r\n\r\n")

        result = self.load_city_data(resp, log_enable = log_enable)
        if(not result):
            return "Error loading city data"

        dt = self.load_and_get_data_updates(resp, log_enable = log_enable)
        if(dt == '0'):
            return "Error loading data updates"

        result = self.load_cities_weather_updates_data(resp, dt, log_enable = log_enable)
        if(not result):
            return "Error loading city weather update"



    def load_city_data(self, data, update_if_exists = False, log_enable = False):
        result = True
        try:
            api_city_id = data.get("id")
            exists = self.data_layer.check_reg_existance("cities", "api_city_id", api_city_id)
            if(not exists):
                lon = data.get("coord", {}).get("lon","NULL")
                lat = data.get("coord", {}).get("lat","NULL")
                country = data.get("coord", {}).get("lat","NULL")
                city_name = data.get("name", "NULL")

                query = ""
                query += "insert into cities(api_city_id, name, longitude, latitude, country) "
                query += "values(%s, '%s', %s, %s, '%s')" % ( str(api_city_id), str(city_name), str(lon), str(lat), str(country) )

                if(log_enable):
                    print("load_city_data:" + str(query), end="\r\n")

                self.data_layer.exec_non_query(query)

        except Exception as ex:
            print(str(ex))
            result = False
        return result


    def load_and_get_data_updates(self, data, log_enable = False):
        result = '0'
        try:
            dt = data["dt"]
            dt = self.unix_time_to_local(dt)
            query = "insert into data_updates(dt) values('%s')" % (str(dt),)

            if(log_enable):
                print("load_and_get_data_updates:" + str(query), end="\r\n")

            self.data_layer.exec_non_query(query)
            query = "select max(id) as id from data_updates"
            result = self.data_layer.exec_query_one(query)["id"]
        except Excepion as ex:
            print(str(ex))
        return result


    def load_cities_weather_updates_data(self, data, data_update_id, update_if_exists = False, log_enable = False):
        result = True
        try:
            api_city_id = data.get("id")
            sunrise = data.get("sys", {}).get("sunrise", "NULL")
            if(sunrise != "NULL"):
                sunrise = self.unix_time_to_local(sunrise)
            sunset = data.get("sys", {}).get("sunset", "NULL")
            if(sunset != "NULL"):
                sunset = self.unix_time_to_local(sunset)
            weather_main = data.get("weather", [{}])[0].get("main", "NULL")
            weather_desc = data.get("weather", [{}])[0].get("description", "NULL")
            icon = data.get("weather", [{}])[0].get("icon", "NULL")
            temp = data.get("main", {}).get("temp", "NULL")
            pressure = data.get("main", {}).get("pressure", "NULL")
            humidity = data.get("main", {}).get("humidity", "NULL")
            temp_min = data.get("main", {}).get("temp_min", "NULL")
            temp_max = data.get("main", {}).get("temp_max", "NULL")
            visibility = data.get("visibility", "NULL")
            wind_speed = data.get("wind", {}).get("speed", "NULL")
            wind_deg = data.get("wind", {}).get("deg", "NULL")
            clouds = data.get("clouds", {}).get("all", "NULL")
            rain = data.get("rain", {}).get("3h", "NULL")

            query = ""
            query += "insert into cities_weather_updates"
            query += "(api_city_id, data_updates_id, sunrise, sunset, "
            query += "weather_main, weather_desc, icon, temp, "
            query += "pressure, humidity, temp_min, temp_max, "
            query += "visibility, wind_speed, wind_deg, clouds, rain) "
            query += "values(%s, %s, '%s', '%s', "  % ( str(api_city_id), str(data_update_id), str(sunrise), str(sunset) )
            query += "'%s', '%s', '%s', %s, " % ( str(weather_main), str(weather_desc), str(icon), str(temp) )
            query += "%s, %s, %s, %s, " % ( str(pressure), str(humidity), str(temp_min), str(temp_max) )
            query += "%s, %s, %s, %s, %s)" % ( str(visibility), str(wind_speed), str(wind_deg), str(clouds), str(rain) )

            if(log_enable):
                print("load_cities_weather_updates_data:" + str(query), end="\r\n")

            self.data_layer.exec_non_query(query)

        except Exception as ex:
            print(str(ex))
            result = False
        return result


    def unix_time_to_local(self, unix_timestamp):
        local_timezone = tzlocal.get_localzone()
        lt = datetime.datetime.fromtimestamp(unix_timestamp, local_timezone)
        resp = "%s-%s-%s %s:%s:%s" % (str(lt.year), str(lt.month), str(lt.day), str(lt.hour), str(lt.minute), str(lt.second))
        return resp
